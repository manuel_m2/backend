const Animals = require('../models/animals');

exports.createAnimal = function (req, res, next) {
    const Animal = {
        name: req.body.name,
        description: req.body.description,
        color: req.body.color,
        age: req.body.age,
    };

    Animals.create(Animal, function(err, Animal) {
        if(err) {
            res.json({
                error : err
            })
        }
        res.json(Animal)
    })
}

exports.getAnimals = function(req, res, next) {
    Animals.get({}, function(err, Animals) {
        if(err) {
            res.json({
                error: err
            })
        }
        res.json({
            Animals: Animals
        })
    })
}

exports.removeAnimal = function(req, res, next) {
    Animals.delete({_id: req.params.id}, function(err, Animal) {
        if(err) {
            res.json({
                error : err
            })
        }
        res.json({
            successfull: true,
            message : "Animal deleted successfully"
        })
    })
}