var mongoose = require('mongoose');
var animalsSchema = require('../schemas/animals');

animalsSchema.statics = {
    create: function(data, cb) {
        var animal = new this(data);
        animal.save(cb);
    },
    get: function(query, cb) {
        this.find(query, cb);
    },
    delete: function(query, cb) {
        this.findOneAndDelete(query,cb);
    }
}

var animalsModel = mongoose.model('animals', animalsSchema);
module.exports = animalsModel;