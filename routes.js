const animals = require('./controllers/animals');

module.exports = function(router) {
    router.get('/animals', animals.getAnimals);
    router.post('/animals', animals.createAnimal);
    router.delete('/animals/:id', animals.removeAnimal);
}