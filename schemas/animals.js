const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const animalsSchema = new Schema({
    name :{
        type: String,
        unique : false,
        required : true
    },
    description : {
        type: String,
        unique : false,
        required : true
    },
    color : {
      type: String,
      unique : false,
      required : false
    },
    age : {
      type: Number,
      unique : false,
      required : false
    }
}, {
    timestamps: true
});

module.exports = animalsSchema;