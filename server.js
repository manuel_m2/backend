const express = require('express');
const cors = require('cors')
const log = require('morgan')('dev');
const bodyParser = require('body-parser');

const db = require('./config/database');
const routes = require('./routes');

const app = express();

const bodyParserJSON = bodyParser.json();
const router = express.Router();

db();

app.use(cors())
app.use(log);
app.use(bodyParserJSON);

app.use('/api',router);

routes(router);

app.listen(5000, (req, res) => {
    console.log(`Server is running on 5000 port.`);
})